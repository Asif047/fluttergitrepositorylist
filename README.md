FlutterGitBuzz
==============

Welcome to FlutterGitBuzz!

Description
-----------

FlutterGitBuzz is a Flutter project designed to display Git repositories retrieved from an API. It filters repositories based on query parameters, with "Flutter" being a key parameter for search results. Additionally, the project supports other query parameters such as page, per\_page, sort, and order during data retrieval.

Features
--------

### HomeScreen:

*   Displays data fetched from API calls, specifically a list of Flutter Git repositories.
*   Implements infinite scrolling, loading 10 additional repositories upon reaching the bottom of the page.
*   Utilizes local SQLite database storage to cache fetched data.
*   Includes a floating action button with four sorting options. Clicking on these options triggers new API calls with updated parameters, and the fetched data is saved in the local database accordingly.

### LocalRepoScreen:

*   Allows users to view data from the local SQLite database when an internet connection is unavailable.
*   Features the same UI as the Home Screen, including the sorting options.

### Details Screen:

*   Provides detailed information about each repository, including the repository owner's image, name, ID, and description.
*   Accessible by clicking on any repository item from the list.

Project Architecture
--------------------

FlutterGitBuzz employs the GetX library for architecture and state management, ensuring efficient and organized code implementation. The project encompasses all the aforementioned features within its architecture.

Libraries Used
--------------

*   **cupertino\_icons:** ^1.0.6
*   **dio:** ^4.0.6
*   **get:** ^4.6.5
*   **lazy\_load\_scrollview:** ^1.1.0
*   **equatable:** ^2.0.5
*   **flutter\_screenutil:** ^5.9.0
*   **shared\_preferences:** ^2.2.2
*   **sqflite:** ^2.3.3
*   **path\_provider:** ^2.1.2
*   **connectivity\_plus:** ^2.0.2
*   **lottie:** ^3.1.0
*   **cached\_network\_image:** ^3.3.1

Installation
------------

To start using FlutterGitBuzz, follow these steps:

1.  Clone the repository to your local machine.
    
    bashCopy code
    
    `git clone <repository-url>`
    
2.  Navigate into the project directory.
    
    bashCopy code
    
    `cd FlutterGitBuzz`
    
3.  Install dependencies.
    
    arduinoCopy code
    
    `flutter pub get`
    
4.  Run the project.
    
    arduinoCopy code
    
    `flutter run`
    

Contributing
------------

Contributions to FlutterGitBuzz are welcome! Feel free to fork the repository, make your changes, and submit a pull request. Please adhere to the project's coding standards and guidelines.

Images
----------

| <img src="/uploads/4f673e7f3f1e711a9a76049710a811bd/img1.jpg" width="200"> | <img src="/uploads/468a974e49ca23b930ba2c8114f8c860/img3.jpg" width="200"> | <img src="/uploads/650e52c69d0c277465879aa759fb772e/img2.jpg" width="200"> |
