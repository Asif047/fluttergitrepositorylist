import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_repository/data/constants.dart';
import 'package:flutter_repository/data/shared_preferences.dart';
import 'package:flutter_repository/db/repository_database.dart';
import 'package:flutter_repository/model/pagination_filter.dart';
import 'package:flutter_repository/model/repository.dart';
import 'package:flutter_repository/model/repository_local.dart';
import 'package:flutter_repository/network/internet_connectivity.dart';
import 'package:flutter_repository/repository/repo_list_repository.dart';
import 'package:get/get.dart';

class RepositoryController extends GetxController {
  final RepoListRepository _repoListRepository;
  final _repo = <Item>[].obs;
  final _paginationFilter = PaginationFilter().obs;
  final _lastPage = false.obs;
  var sort = "stars".obs;
  var order = "asc".obs;
  List<RepositoryLocal> repoLocal = <RepositoryLocal>[].obs;

  final RxList repoList = <RepositoryLocal>[].obs;

  Future<void> getLocalRepo() async {
    final List<Map<String, dynamic>> repos =
        await RepositoryDatabase.instance.queryAllRows();
    repoList.clear();
    repoList.assignAll(
        repos.map((data) => RepositoryLocal.fromJson(data)).toList());
  }

  Future<void> getLocalRepoByStarCountAsc() async {
    final List<RepositoryLocal> repos =
        await RepositoryDatabase.instance.getRepositoriesByStarCountAscending();
    repoList.clear();
    repoList.assignAll(repos);
  }

  Future<void> getLocalRepoByStarCountDesc() async {
    final List<RepositoryLocal> repos = await RepositoryDatabase.instance
        .getRepositoriesByStarCountDescending();
    repoList.clear();
    repoList.assignAll(repos);
  }

  Future<void> getLocalRepoByDateCountDesc() async {
    final List<RepositoryLocal> repos = await RepositoryDatabase.instance
        .getRepositoriesByUpdatedAtDescending();
    repoList.clear();
    repoList.assignAll(repos);
  }

  Future<void> getLocalRepoByDateCountASC() async {
    final List<RepositoryLocal> repos =
        await RepositoryDatabase.instance.getRepositoriesByUpdatedAtAscending();
    repoList.clear();
    repoList.assignAll(repos);
  }

  //for pagination

  RepositoryController(this._repoListRepository);

  List<Item> get repo => _repo.toList();

  int get limit => _paginationFilter.value.perPage;

  int get _page => _paginationFilter.value.page;

  bool get lastPage => _lastPage.value;

  ScrollController scrollController = ScrollController();
  var isMoreDataAvailable = true.obs;

  Map _source = {ConnectivityResult.none: false};
  final InternetConnectivity _connectivity = InternetConnectivity.instance;

  late StreamSubscription _subscription;
  bool flag = true;

  @override
  onInit() async {
    SharedPreferencesHelper preferencesHelper =
        await SharedPreferencesHelper.getInstance();
    String? sortPref = preferencesHelper.getData(AppConstant.SORT_KEY);
    String? orderPref = preferencesHelper.getData(AppConstant.ORDER_KEY);

    if (sortPref != null) {
      sort.value = sortPref;
    }

    if (orderPref != null) {
      order.value = orderPref;
    }

    _connectivity.initialise();
    _subscription = _connectivity.myStream.listen((source) {
      // Check if the widget is still mounted

      _source = source;
      if (_source.keys.toList()[0] != ConnectivityResult.none) {
        // Start a 5-second timer to navigate if the device remains offline
        Future.delayed(Duration(seconds: 3), () {
          if (_source.keys.toList()[0] != ConnectivityResult.none) {
            // Check again if the widget is still mounted
            // If still offline after 5 seconds, navigate
            if (flag) {
              ever(_paginationFilter, (_) => _getAllRepo());
              _changePaginationFilter(1, 10, sort.value, order.value);
              flag = false;
            }
          }
        });
      }
    });

    super.onInit();
  }

  Future<void> _getAllRepo() async {
    final repoData =
        await _repoListRepository.getRepositories(_paginationFilter.value);

    if (repoData.items.isEmpty) {
      _lastPage.value = true;
    }
    //_users.addAll(usersData);

    if (repoData.items.length > 0) {
      isMoreDataAvailable(true);
    } else {
      isMoreDataAvailable(false);
    }

    for (int i = 0; i < repoData.items.length; i++) {
      _repo.add(repoData.items[i]);

      final exists = await RepositoryDatabase.instance
          .repositoryExists(repoData.items[i].id.toString());
      if (exists) {
        // Repository exists
      } else {
        // Repository does not exist
        RepositoryLocal repositoryLocal = RepositoryLocal(
            repoId: repoData.items[i].id.toString(),
            fullName: repoData.items[i].fullName,
            ownerLogin: repoData.items[i].owner.login,
            ownerId: repoData.items[i].owner.id.toString(),
            ownerAvatarUrl: repoData.items[i].owner.avatarUrl,
            description: repoData.items[i].description,
            createdAt: repoData.items[i].createdAt,
            updatedAt: repoData.items[i].updatedAt,
            pushedAt: repoData.items[i].pushedAt,
            starCount: repoData.items[i].stargazersCount.toString(),
            forks: repoData.items[i].forksCount.toString(),
            watchers: repoData.items[i].watchersCount.toString(),
            score: repoData.items[i].stargazersCount.toString(),
            visibility: repoData.items[i].forksUrl);
        RepositoryDatabase.instance.create(repositoryLocal);
      }
    }
  }

  void changeTotalPerPage(int limitValue, String sort, String order) {
    _repo.clear();
    _lastPage.value = false;
    _changePaginationFilter(1, limitValue, sort, order);
  }

  void _changePaginationFilter(
      int page, int perPage, String sort, String order) {
    _paginationFilter.update((val) {
      val!.page = page;
      val.perPage = perPage;
      val.sort = sort;
      val.order = order;
    });
  }

  void loadNextPage() =>
      _changePaginationFilter(_page + 1, limit, sort.value, order.value);
}
