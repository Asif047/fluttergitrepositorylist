mixin URLs {
  static const String host = 'https://api.github.com/';
  static const String repositories = '${host}repositories';
}

mixin AppConstant {
  static const String SORT_KEY = 'sort';
  static const String ORDER_KEY = 'order';
  static const int DATA_LIMIT = 10;
}

// constant for page limit & timeout
mixin AppLimit {
  static const int REQUEST_TIME_OUT = 30000;
}

const String appVersion = '0.0.1';
const String environment = 'Production';