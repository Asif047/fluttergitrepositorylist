import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesHelper {
  static SharedPreferencesHelper? _instance;
  static SharedPreferences? _preferences;

  static Future<SharedPreferencesHelper> getInstance() async {
    if (_instance == null) {
      _instance = SharedPreferencesHelper();
      await _instance!._initPreferences();
    }
    return _instance!;
  }

  Future<void> _initPreferences() async {
    _preferences = await SharedPreferences.getInstance();
  }

  // Save a string value to shared preferences
  Future<bool> saveData(String key, String value) {
    return _preferences!.setString(key, value);
  }

  // Retrieve a string value from shared preferences
  String? getData(String key) {
    return _preferences?.getString(key);
  }

  // Remove a value from shared preferences
  Future<bool> remove(String key) {
    return _preferences!.remove(key);
  }
}
