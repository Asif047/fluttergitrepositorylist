import 'package:flutter/material.dart';

Text getTitleText(String title, double fontSize) {
  return Text(
    title,
    style: TextStyle(
        fontWeight: FontWeight.w400, fontFamily: "salsa", fontSize: fontSize),
  );
}


Text getRepoIdText(String title, double fontSize) {
  return Text(
    title,
    style: TextStyle(
        fontSize: fontSize,
        fontFamily: 'roboto_slab'),
  );
}
