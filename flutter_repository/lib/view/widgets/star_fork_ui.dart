import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

Widget getStarForkUI(IconData icon, String count, Color color) {
  return Row(
    children: [
      Icon(
        icon,
        color: color,
      ),
      SizedBox(
        width: 10.w,
      ),
      Text(
        count,
        style: TextStyle(
            fontSize: 14.sp,
            fontFamily: 'roboto_slab'),
      )
    ],
  );
}