import 'package:flutter/material.dart';
import 'package:flutter_repository/controller/repository_controller.dart';
import 'package:flutter_repository/data/constants.dart';
import 'package:flutter_repository/data/shared_preferences.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

Future<void> showSortModal(
    BuildContext context, RepositoryController _controller) {
  return showModalBottomSheet<void>(
    context: context,
    builder: (BuildContext context) {
      return SizedBox(
        height: 300.h,
        child: Center(
          child: Column(
            children: <Widget>[
              InkWell(
                child: getModalText("Sort by Stars ASC"),
                onTap: () async {
                  _controller.sort.value = "stars";
                  _controller.order.value = "asc";

                  _controller.changeTotalPerPage(
                      10, _controller.sort.value, _controller.order.value);

                  saveData(_controller);
                  _controller.getLocalRepoByStarCountAsc();
                  Navigator.pop(context);
                },
              ),
              getDivider(),
              InkWell(
                child: getModalText("Sort by Stars DESC"),
                onTap: () {
                  _controller.sort.value = "stars";
                  _controller.order.value = "desc";

                  _controller.changeTotalPerPage(
                      10, _controller.sort.value, _controller.order.value);
                  saveData(_controller);
                  _controller.getLocalRepoByStarCountDesc();
                  Navigator.pop(context);
                },
              ),
              getDivider(),
              InkWell(
                child: getModalText(
                  "Sort by Update ASC",
                ),
                onTap: () {
                  _controller.sort.value = "updated";
                  _controller.order.value = "asc";

                  _controller.changeTotalPerPage(
                      10, _controller.sort.value, _controller.order.value);
                  saveData(_controller);
                  _controller.getLocalRepoByDateCountASC();
                  Navigator.pop(context);
                },
              ),
              getDivider(),
              InkWell(
                child: getModalText("Sort by Update DESC"),
                onTap: () {
                  _controller.sort.value = "updated";
                  _controller.order.value = "desc";

                  _controller.changeTotalPerPage(
                      10, _controller.sort.value, _controller.order.value);
                  saveData(_controller);
                  _controller.getLocalRepoByDateCountDesc();
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        ),
      );
    },
  );
}

Future<void> saveData(RepositoryController _controller) async {
  SharedPreferencesHelper preferencesHelper =
      await SharedPreferencesHelper.getInstance();
  preferencesHelper.saveData(AppConstant.SORT_KEY, _controller.sort.value);
  preferencesHelper.saveData(AppConstant.ORDER_KEY, _controller.order.value);
}

Widget getModalText(String title) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    mainAxisSize: MainAxisSize.max,
    children: [
      Padding(
        padding: EdgeInsets.all(10.w),
        child: Text(
          title,
          style: TextStyle(fontSize: 18.sp, fontWeight: FontWeight.w500),
        ),
      ),
    ],
  );
}

Widget getDivider() {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Divider(
      height: 2.h,
      color: Colors.blue,
    ),
  );
}
