import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Timer(Duration(seconds: 3), () {
      Get.offAllNamed('/homeScreen');
    });

    return MaterialApp(
      home: Scaffold(
        body: Center(
            // Add your splash screen UI components here
            child: Padding(
          padding: EdgeInsets.all(20.h),
          child: Lottie.asset("assets/animation/git_animation.json"),
        )),
      ),
    );
  }
}
