import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_repository/controller/repository_controller.dart';
import 'package:flutter_repository/data/constants.dart';
import 'package:flutter_repository/data/shared_preferences.dart';
import 'package:flutter_repository/network/internet_connectivity.dart';
import 'package:flutter_repository/view/home_screen.dart';
import 'package:flutter_repository/view/widgets/common_text_widget.dart';
import 'package:flutter_repository/view/widgets/sort_modal_widget.dart';
import 'package:flutter_repository/view/widgets/star_fork_ui.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:lottie/lottie.dart';

class LocalRepoScreen extends StatefulWidget {
  final RepositoryController _controller;

  const LocalRepoScreen(this._controller);

  @override
  State<LocalRepoScreen> createState() => LocalRepoScreenState(_controller);
}

class LocalRepoScreenState extends State<LocalRepoScreen> {
  final RepositoryController _controller;

  LocalRepoScreenState(this._controller);

  var sort = "stars".obs;
  var order = "asc".obs;

  Map _source = {ConnectivityResult.none: false};
  final InternetConnectivity _connectivity = InternetConnectivity.instance;

  late StreamSubscription _subscription;

  @override
  void initState() {
    super.initState();
    getPrefData();
    _connectivity.initialise();
    _subscription = _connectivity.myStream.listen((source) {
      if (mounted) {
        // Check if the widget is still mounted
        setState(() {
          _source = source;
          if (_source.keys.toList()[0] != ConnectivityResult.none) {
            // Start a 5-second timer to navigate if the device remains offline
            Future.delayed(Duration(seconds: 3), () {
              if (mounted &&
                  _source.keys.toList()[0] != ConnectivityResult.none) {
                // Check again if the widget is still mounted
                // If still offline after 5 seconds, navigate
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => HomeScreen(_controller)),
                );

                // _controller.changeTotalPerPage(AppConstant.DATA_LIMIT, _controller.sort.value, _controller.order.value);
              }
            });
          }
        });
      }
    });
  }

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }

  Future<void> getPrefData() async {
    SharedPreferencesHelper preferencesHelper =
        await SharedPreferencesHelper.getInstance();

    String? sortPref = preferencesHelper.getData(AppConstant.SORT_KEY);
    String? orderPref = preferencesHelper.getData(AppConstant.ORDER_KEY);

    if (sortPref != null) {
      sort.value = sortPref;
    }

    if (orderPref != null) {
      order.value = orderPref;
    }

    if (sort.value == "stars" && order.value == "asc") {
      _controller.getLocalRepoByStarCountAsc();
    }

    if (sort.value == "stars" && order.value == "desc") {
      _controller.getLocalRepoByStarCountDesc();
    }

    if (sort.value == "updated" && order.value == "asc") {
      _controller.getLocalRepoByDateCountASC();
    }

    if (sort.value == "updated" && order.value == "desc") {
      _controller.getLocalRepoByDateCountDesc();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: EdgeInsets.all(10.h),
          child: Text(
            "Repositories(Offline)",
            style: TextStyle(
                fontSize: 18.sp,
                fontFamily: 'salsa',
                fontWeight: FontWeight.w500),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _controller.sort.value = "hello";

          debugPrint("##VALUE2: ${_controller.sort.value}");

          showSortModal(context, _controller);
        },
        child: const Icon(Icons.sort),
      ),
      body: Obx(() {
        if (_controller.repoList.isEmpty) {
          return Lottie.asset("assets/animation/no_data_anim.json");
        } else {
          return Column(
            children: [
              Expanded(
                child: LazyLoadScrollView(
                    onEndOfPage: _controller.loadNextPage,
                    child: ListView.builder(
                      itemCount: _controller.repoList.length,
                      itemBuilder: (context, index) {
                        final repo = _controller.repoList[index];

                        return Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: InkWell(
                            onTap: () {
                              Get.toNamed('/detailsScreen', arguments: {
                                'owner_name': '${repo.ownerLogin}',
                                'repo_name': '${repo.fullName}',
                                'photo': '${repo.ownerAvatarUrl}',
                                'desc': '${repo.description}',
                                'last_update': '${repo.updatedAt}'
                              });
                            },
                            child: Card(
                              child: Padding(
                                padding: EdgeInsets.all(10.h),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [

                                        ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(100.h),
                                          child: CachedNetworkImage(
                                            imageUrl: repo.ownerAvatarUrl,
                                            height: 50.h,
                                            width: 50.w,
                                            fit: BoxFit.fill,
                                            placeholder: (context, url) =>
                                                const CircularProgressIndicator(),
                                            errorWidget:
                                                (context, url, error) =>
                                                    const Icon(Icons.error),
                                          ),
                                        ),

                                        SizedBox(
                                          width: 20.w,
                                        ),

                                        Expanded(
                                            child: Text(
                                          repo.fullName,
                                          style: TextStyle(
                                              fontSize: 16.sp,
                                              fontWeight: FontWeight.w500),
                                        ))
                                      ],
                                    ),
                                    SizedBox(
                                      height: 6.h,
                                    ),
                                    Row(
                                      children: [

                                        getTitleText("Repository Id:", 18.sp),

                                        SizedBox(
                                          width: 20.w,
                                        ),
                                        getRepoIdText(repo.repoId, 16.sp),

                                      ],
                                    ),
                                    SizedBox(
                                      height: 6.h,
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        getStarForkUI(Icons.star, '${repo.starCount}', Colors.orange),
                                        getStarForkUI(Icons.fork_left, '${repo.forks}', Colors.grey),

                                      ],
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    )),
              ),
            ],
          );
        }
      }),
    );
  }
}
