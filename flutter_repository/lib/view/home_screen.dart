import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_repository/controller/repository_controller.dart';
import 'package:flutter_repository/data/shared_preferences.dart';
import 'package:flutter_repository/view/local_repo_screen.dart';
import 'package:flutter_repository/view/widgets/common_text_widget.dart';
import 'package:flutter_repository/view/widgets/sort_modal_widget.dart';
import 'package:flutter_repository/view/widgets/star_fork_ui.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

import '../network/internet_connectivity.dart';

class HomeScreen extends StatefulWidget {
  final RepositoryController _controller;

  const HomeScreen(this._controller);

  @override
  State<HomeScreen> createState() => _HomeScreenState(_controller);
}

class _HomeScreenState extends State<HomeScreen> {
  final RepositoryController _controller;
  late SharedPreferencesHelper preferencesHelper;

  _HomeScreenState(this._controller);

  Map _source = {ConnectivityResult.none: false};
  final InternetConnectivity _connectivity = InternetConnectivity.instance;

  late StreamSubscription _subscription;
  late Timer _timer;

  @override
  void initState() {
    super.initState();
    _connectivity.initialise();
    _subscription = _connectivity.myStream.listen((source) {
      if (mounted) {
        // Check if the widget is still mounted
        setState(() {
          _source = source;
          if (_source.keys.toList()[0] == ConnectivityResult.none) {
            // Start a 5-second timer to navigate if the device remains offline
            Future.delayed(Duration(seconds: 3), () {
              if (mounted &&
                  _source.keys.toList()[0] == ConnectivityResult.none) {
                // Check again if the widget is still mounted
                // If still offline after 5 seconds, navigate
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => LocalRepoScreen(_controller)),
                );
              }
            });
          }
        });
      }
    });

    // Start a periodic timer to call the function every 30 seconds
    _timer = Timer.periodic(Duration(minutes: 30), (timer) {
      _controller.repo.clear();
      _controller.changeTotalPerPage(10, _controller.sort.value, _controller.order.value);
    });
  }

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }

  bool getLoadLastPage() {
    return _controller.lastPage;
  }

  bool getNextPage() {
    return _controller.lastPage;
  }

  @override
  Widget build(BuildContext context) {
    String string;
    switch (_source.keys.toList()[0]) {
      case ConnectivityResult.mobile:
        string = 'Mobile: Online';
        break;
      case ConnectivityResult.wifi:
        string = 'WiFi: Online';
        break;
      case ConnectivityResult.none:
      default:
        string = 'Offline';
    }

    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: EdgeInsets.all(10.h),
          child: Text(
            "Repositories",
            style: TextStyle(
                fontSize: 18.sp,
                fontFamily: 'salsa',
                fontWeight: FontWeight.w500),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _controller.sort.value = "stars";

          showSortModal(context, _controller);
        },
        child: const Icon(Icons.sort),
      ),
      body: Obx(() => Column(
            children: [
              Expanded(
                child: LazyLoadScrollView(
                    onEndOfPage: _controller.loadNextPage,
                    isLoading: getLoadLastPage(),
                    child: ListView.builder(
                      itemCount: _controller.repo.length,
                      itemBuilder: (context, index) {
                        final repo = _controller.repo[index];

                        if (index == _controller.repo.length - 1 &&
                            _controller.isMoreDataAvailable == true) {
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        }

                        return Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: InkWell(
                            onTap: () {
                              Get.toNamed('/detailsScreen', arguments: {
                                'owner_name': '${repo.owner.login}',
                                'repo_name': '${repo.fullName}',
                                'photo': '${repo.owner.avatarUrl}online',
                                'desc': '${repo.description}',
                                'last_update': '${repo.updatedAt}'
                              });
                            },
                            child: Card(
                              child: Padding(
                                padding: EdgeInsets.all(10.h),
                                child: Container(
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(100.h),
                                            child: CachedNetworkImage(
                                              imageUrl: repo.owner.avatarUrl,
                                              height: 50.h,
                                              width: 50.w,
                                              fit: BoxFit.fill,
                                              placeholder: (context, url) =>
                                                  const CircularProgressIndicator(),
                                              errorWidget:
                                                  (context, url, error) =>
                                                      const Icon(Icons.error),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 20.w,
                                          ),
                                          Expanded(
                                              child: Text(
                                            repo.fullName,
                                            style: TextStyle(
                                                fontSize: 16.sp,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: 'vollkorn'),
                                          ))
                                        ],
                                      ),
                                      SizedBox(
                                        height: 6.h,
                                      ),
                                      Row(
                                        children: [
                                          getTitleText("Repository Id:", 18.sp),

                                          SizedBox(
                                            width: 20.w,
                                          ),
                                          getRepoIdText(repo.id.toString(), 16.sp)

                                        ],
                                      ),
                                      SizedBox(
                                        height: 6.h,
                                      ),
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          getStarForkUI(Icons.star, '${repo.stargazersCount}', Colors.orange),
                                          getStarForkUI(Icons.fork_left, '${repo.forksCount}', Colors.grey),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    )),
              ),

              //CircularProgressIndicator()
            ],
          )),
    );
  }
}
