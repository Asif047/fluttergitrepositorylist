import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_repository/view/widgets/common_text_widget.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class DetailsScreen extends StatefulWidget {
  const DetailsScreen({super.key});

  @override
  State<DetailsScreen> createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  final Map<String, dynamic> args = Get.arguments;
  String formattedDateTime = "";

  String formatDate(String dateTimeString) {
    // Parse the datetime string into a DateTime object
    DateTime dateTime = DateTime.parse(dateTimeString);

    // Format the DateTime object into the desired format
    String formattedDateTime = "${dateTime.month.toString().padLeft(2, '0')}-"
        "${dateTime.day.toString().padLeft(2, '0')}-"
        "${dateTime.year}-${dateTime.hour.toString().padLeft(2, '0')}:"
        "${dateTime.minute.toString().padLeft(2, '0')}";

    return formattedDateTime;
  }

  @override
  void initState() {
    super.initState();

    String dateTimeString = args['last_update'];
    formattedDateTime = formatDate(dateTimeString);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: EdgeInsets.all(10.h),
          child: Text(
            "Repository Details",
            style: TextStyle(
                fontSize: 18.sp,
                fontFamily: 'salsa',
                fontWeight: FontWeight.w500),
          ),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.all(20.h),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 20.h,
              ),
              ClipRRect(
                  borderRadius: BorderRadius.circular(100.h),
                  child: FutureBuilder<Widget>(
                    future: loadImage(args['photo']),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return CircularProgressIndicator();
                      } else if (snapshot.hasError) {
                        return Text('Failed to load image: ${snapshot.error}');
                      } else {
                        return snapshot.data ?? Text('Image not available');
                      }
                    },
                  )),
              SizedBox(
                height: 10.h,
              ),
              Text(
                args['owner_name'],
                style: TextStyle(fontSize: 20.sp, fontWeight: FontWeight.w500, color: Colors.deepPurpleAccent),
              ),

              SizedBox(
                height: 20.h,
              ),

              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  getTitleText('Repository name:', 16.sp),

                  SizedBox(
                    height: 4.h,
                  ),
                  Text(
                    args['repo_name'],
                    style: TextStyle(
                      fontSize: 14.sp,
                      fontFamily: 'roboto_slab',
                      color: Colors.blueGrey
                    ),
                  ),
                  SizedBox(
                    height: 20.h,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      getTitleText('Updated at:', 16.sp),
                      SizedBox(
                        width: 20.w,
                      ),
                      Expanded(
                          child: Padding(
                        padding:  EdgeInsets.all(2.w),
                        child: Text(
                          formattedDateTime,
                          style: TextStyle(
                            fontSize: 14.sp,
                              fontFamily: 'roboto_slab',
                              color: Colors.blueGrey
                          ),
                        ),
                      ))
                    ],
                  ),
                  SizedBox(
                    height: 20.h,
                  ),
                  getTitleText('Description: ', 16.sp),
                  SizedBox(
                    height: 4.h,
                  ),
                  Text(args['desc'],
                      style: TextStyle(
                        fontSize: 14.sp,
                        fontFamily: 'fira_sans',
                          color: Colors.blueGrey
                      )),


                ],
              )
              //Text(args['last_update']),
            ],
          ),
        ),
      ),
    );
  }

  Future<Widget> loadImage(String imageUrl) async {
    try {
      return CachedNetworkImage(
        imageUrl: imageUrl,
        height: 100.h,
        width: 100.w,
        fit: BoxFit.fill,
        placeholder: (context, url) => CircularProgressIndicator(),
        errorWidget: (context, url, error) => Icon(Icons.error),
      );
    } on SocketException catch (e) {
      // Handle the case where the device is offline
      //print('SocketException: $e');
      return Text(
          'Unable to load image. Please check your internet connection.');
    } catch (e) {
      // Handle any other exceptions
      //print('Exception: $e');
      return Text('An error occurred while loading the image.');
    }
  }
}
