import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_repository/model/pagination_filter.dart';
import 'package:flutter_repository/model/repository.dart';

class RepoListRepository {
  final Dio _dio;

  RepoListRepository(this._dio);

  Future<Repository> getRepositories(PaginationFilter filter) async {
    try {
      final response = await _dio.get('search/repositories', queryParameters: {
        'q': 'flutter',
        'page': filter.page,
        'per_page': filter.perPage,
        'sort': filter.sort,
        'order': filter.order
      });

      return Repository.fromJson(response.data);
    } on DioError catch (e) {
      // Handle Dio errors
      //debugPrint("###QQQ: DioError - ${e.message}");
      if (e.type == DioErrorType.other && e.error is SocketException) {
        throw Exception("Failed to fetch repo: No internet connection");
      } else {
        throw Exception("Failed to fetch repo: ${e.message}");
      }
    } catch (e) {
      // Handle other errors
      //debugPrint("###QQQ: Other Error - $e");
      throw Exception("Failed to fetch repo: ${e.toString()}");
    }
  }
}
