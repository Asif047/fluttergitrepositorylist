import 'package:flutter_repository/model/repository_local.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class RepositoryDatabase {
  static final RepositoryDatabase instance = RepositoryDatabase._init();

  static Database? _database;

  RepositoryDatabase._init();

  Future<Database> get database async {
    if (_database != null) return _database!;

    _database = await _initDB('repository.db');
    return _database!;
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);

    return await openDatabase(path, version: 1, onCreate: _createDB);
  }

  Future _createDB(Database db, int version) async {
    final idType = 'INTEGER PRIMARY KEY AUTOINCREMENT';
    final repoIdType = 'TEXT NOT NULL';
    final fullNameType = 'TEXT NOT NULL';
    final ownerLoginType = 'TEXT NOT NULL';
    final ownerIdType = 'TEXT NOT NULL';
    final ownerAvatarType = 'TEXT NOT NULL';
    final descriptionType = 'TEXT NOT NULL';
    final createdAtType = 'TEXT NOT NULL';
    final updatedAtType = 'TEXT NOT NULL';
    final pushedAtType = 'TEXT NOT NULL';
    final starCountType = 'TEXT NOT NULL';
    final forksType = 'TEXT NOT NULL';
    final watchersType = 'TEXT NOT NULL';
    final scoreType = 'TEXT NOT NULL';
    final visibilityType = 'TEXT NOT NULL';

    await db.execute('''
      CREATE TABLE $tableRepository ( 
      ${RepositoryFields.id} $idType,
      ${RepositoryFields.repoId} $repoIdType,
      ${RepositoryFields.fullName} $fullNameType,
      ${RepositoryFields.ownerLogin} $ownerLoginType,
      ${RepositoryFields.ownerId} $ownerIdType,
      ${RepositoryFields.ownerAvatarUrl} $ownerAvatarType,
      ${RepositoryFields.description} $descriptionType,
      ${RepositoryFields.createdAt} $createdAtType,
      ${RepositoryFields.updatedAt} $updatedAtType,
      ${RepositoryFields.pushedAt} $pushedAtType,
      ${RepositoryFields.starCount} $starCountType,
      ${RepositoryFields.forks} $forksType,
      ${RepositoryFields.watchers} $watchersType,
      ${RepositoryFields.score} $scoreType,
      ${RepositoryFields.visibility} $visibilityType
      )
    ''');
  }

  Future<RepositoryLocal> create(RepositoryLocal repositoryLocal) async {
    final db = await instance.database;
    try {
      final id = await db.insert(tableRepository, repositoryLocal.toJson());
      return repositoryLocal.copy(id: id);
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<bool> repositoryExists(String repoId) async {
    final db = await instance.database;

    final count = Sqflite.firstIntValue(await db.rawQuery(
      'SELECT COUNT(*) FROM $tableRepository WHERE ${RepositoryFields.repoId} = ?',
      [repoId],
    ));

    return count! > 0;
  }

  Future<RepositoryLocal> readRepo(int id) async {
    final db = await instance.database;

    final maps = await db.query(
      tableRepository,
      columns: RepositoryFields.values,
      where: '${RepositoryFields.id} = ?',
      whereArgs: [id],
    );

    if (maps.isNotEmpty) {
      return RepositoryLocal.fromJson(maps.first);
    } else {
      throw Exception('ID $id not found');
    }
  }

  Future<List<Map<String, dynamic>>> queryAllRows() async {
    final db = await instance.database;
    return await db!.query(tableRepository);
  }

  Future<List<RepositoryLocal>> getRepositoriesByStarCountAscending() async {
    final db = await instance.database;
    final List<Map<String, dynamic>> result = await db.query(
      tableRepository,
      orderBy: 'CAST(${RepositoryFields.starCount} AS INTEGER) ASC',
    );

    return result.map((json) => RepositoryLocal.fromJson(json)).toList();
  }

  Future<List<RepositoryLocal>> getRepositoriesByStarCountDescending() async {
    final db = await instance.database;
    final List<Map<String, dynamic>> result = await db.query(
      tableRepository,
      orderBy: 'CAST(${RepositoryFields.starCount} AS INTEGER) DESC',
    );

    return result.map((json) => RepositoryLocal.fromJson(json)).toList();
  }

  Future<List<RepositoryLocal>> getRepositoriesByUpdatedAtAscending() async {
    final db = await instance.database;
    final List<Map<String, dynamic>> result = await db.query(
      tableRepository,
      orderBy: '${RepositoryFields.updatedAt} ASC',
    );

    return result.map((json) => RepositoryLocal.fromJson(json)).toList();
  }

  Future<List<RepositoryLocal>> getRepositoriesByUpdatedAtDescending() async {
    final db = await instance.database;
    final List<Map<String, dynamic>> result = await db.query(
      tableRepository,
      orderBy: '${RepositoryFields.updatedAt} DESC',
    );

    return result.map((json) => RepositoryLocal.fromJson(json)).toList();
  }

  Future<List<RepositoryLocal>> readAllRepo() async {
    final db = await instance.database;
    final result = await db.query(tableRepository);
    return result.map((json) => RepositoryLocal.fromJson(json)).toList();
  }

  Future<int> update(RepositoryLocal repositoryLocal) async {
    final db = await instance.database;

    return db.update(
      tableRepository,
      repositoryLocal.toJson(),
      where: '${RepositoryFields.id} = ?',
      whereArgs: [repositoryLocal.id],
    );
  }

  Future<int> delete(int id) async {
    final db = await instance.database;
    return await db.delete(
      tableRepository,
      where: '${RepositoryFields.id} = ?',
      whereArgs: [id],
    );
  }

  Future close() async {
    final db = await instance.database;
    db.close();
  }
}
