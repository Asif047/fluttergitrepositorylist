import 'package:flutter/material.dart';
import 'package:flutter_repository/bindings/application_bindings.dart';
import 'package:flutter_repository/bindings/repository_bindings.dart';
import 'package:flutter_repository/view/details_screen.dart';
import 'package:flutter_repository/view/home_screen.dart';
import 'package:flutter_repository/view/local_repo_screen.dart';
import 'package:flutter_repository/view/splash_screen.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: const Size(360, 690),
        minTextAdapt: true,
        splitScreenMode: true,
        builder: (_, child) {
          return GetMaterialApp(
            debugShowCheckedModeBanner: false,
            initialBinding: ApplicationBinding(),
            title: 'Flutter Demo',
            theme: ThemeData(
              colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
              useMaterial3: true,
            ),
            initialRoute: '/',
            getPages: [
              GetPage(
                name: '/',
                page: () => SplashScreen(),
              ),
              GetPage(
                  name: '/homeScreen',
                  page: () => HomeScreen(Get.find()),
                  binding: RepositoryBindings()),
              GetPage(
                  name: '/localRepo',
                  page: () => LocalRepoScreen(Get.find()),
                  binding: RepositoryBindings()),
              GetPage(
                name: '/detailsScreen',
                page: () => DetailsScreen(),
              ),
            ],
          );
        });
  }
}
