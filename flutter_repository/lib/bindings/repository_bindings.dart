import 'package:flutter_repository/controller/repository_controller.dart';
import 'package:flutter_repository/repository/repo_list_repository.dart';
import 'package:get/get.dart';

class RepositoryBindings implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => RepoListRepository(Get.find()));
    Get.lazyPut(() => RepositoryController(Get.find()));
  }
}
