final String tableRepository = 'repository';

class RepositoryFields {
  static final List<String> values = [
    /// Add all fields
    id,
    fullName,
    ownerLogin,
    ownerId,
    ownerAvatarUrl,
    description,
    createdAt,
    updatedAt,
    pushedAt,
    starCount,
    forks,
    watchers,
    score,
    visibility
  ];

  static final String id = '_id';
  static final String repoId = 'repo_id';
  static final String fullName = 'full_name';
  static final String ownerLogin = 'owner_login';
  static final String ownerId = 'owner_id';
  static final String ownerAvatarUrl = 'owner_avatar_url';
  static final String description = 'description';
  static final String createdAt = 'created_at';
  static final String updatedAt = 'updated_at';
  static final String pushedAt = 'pushed_at';
  static final String starCount = 'star_count';
  static final String forks = 'fokrs';
  static final String watchers = 'watchers';
  static final String score = 'score';
  static final String visibility = 'visibility';
}

class RepositoryLocal {
  final int? id;
  final String repoId;
  final String fullName;
  final String ownerLogin;
  final String ownerId;
  final String ownerAvatarUrl;
  final String description;
  final String createdAt;
  final String updatedAt;
  final String pushedAt;
  final String starCount;
  final String forks;
  final String watchers;
  final String score;
  final String visibility;

  const RepositoryLocal(
      {this.id,
      required this.repoId,
      required this.fullName,
      required this.ownerLogin,
      required this.ownerId,
      required this.ownerAvatarUrl,
      required this.description,
      required this.createdAt,
      required this.updatedAt,
      required this.pushedAt,
      required this.starCount,
      required this.forks,
      required this.watchers,
      required this.score,
      required this.visibility});

  RepositoryLocal copy(
          {int? id,
          bool? isImportant,
          String? repoId,
          String? fullName,
          String? ownerLogin,
          String? ownerId,
          String? ownerAvatarUrl,
          String? description,
          String? createdAt,
          String? updatedAt,
          String? pushedAt,
          String? starCount,
          String? forks,
          String? watchers,
          String? score,
          String? visibility}) =>
      RepositoryLocal(
        id: id ?? this.id,
        repoId: repoId ?? this.repoId,
        fullName: fullName ?? this.fullName,
        ownerLogin: description ?? this.ownerLogin,
        ownerId: description ?? this.ownerId,
        ownerAvatarUrl: description ?? this.ownerAvatarUrl,
        description: description ?? this.description,
        createdAt: description ?? this.createdAt,
        updatedAt: description ?? this.updatedAt,
        pushedAt: description ?? this.pushedAt,
        starCount: description ?? this.starCount,
        forks: description ?? this.forks,
        watchers: description ?? this.watchers,
        score: description ?? this.score,
        visibility: description ?? this.visibility,
      );

  static RepositoryLocal fromJson(Map<String, Object?> json) => RepositoryLocal(
        id: json[RepositoryFields.id] as int?,
        repoId: json[RepositoryFields.repoId] as String,
        fullName: json[RepositoryFields.fullName] as String,
        ownerLogin: json[RepositoryFields.ownerLogin] as String,
        ownerId: json[RepositoryFields.ownerId] as String,
        ownerAvatarUrl: json[RepositoryFields.ownerAvatarUrl] as String,
        description: json[RepositoryFields.description] as String,
        createdAt: json[RepositoryFields.createdAt] as String,
        updatedAt: json[RepositoryFields.updatedAt] as String,
        pushedAt: json[RepositoryFields.pushedAt] as String,
        starCount: json[RepositoryFields.starCount] as String,
        forks: json[RepositoryFields.forks] as String,
        watchers: json[RepositoryFields.watchers] as String,
        score: json[RepositoryFields.score] as String,
        visibility: json[RepositoryFields.visibility] as String,
      );

  Map<String, Object?> toJson() => {
        RepositoryFields.id: id,
        RepositoryFields.repoId: repoId,
        RepositoryFields.fullName: fullName,
        RepositoryFields.ownerLogin: ownerLogin,
        RepositoryFields.ownerId: ownerId,
        RepositoryFields.ownerAvatarUrl: ownerAvatarUrl,
        RepositoryFields.description: description,
        RepositoryFields.createdAt: createdAt,
        RepositoryFields.updatedAt: updatedAt,
        RepositoryFields.pushedAt: pushedAt,
        RepositoryFields.starCount: starCount,
        RepositoryFields.forks: forks,
        RepositoryFields.watchers: watchers,
        RepositoryFields.score: score,
        RepositoryFields.visibility: visibility
      };
}
