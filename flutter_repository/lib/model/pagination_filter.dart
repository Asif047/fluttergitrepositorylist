class PaginationFilter {
  int page = 0;
  int perPage = 0;
  String sort= "stars";
  String order= "asc";

  @override
  String toString() => 'PaginationFilter(page: $page, limit: $perPage, sort: $sort, order: $order)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is PaginationFilter && o.page == page && o.perPage == perPage;
  }

  @override
  int get hashCode => page.hashCode ^ perPage.hashCode;
}