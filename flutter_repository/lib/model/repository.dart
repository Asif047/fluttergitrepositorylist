import 'dart:convert';

Repository repositoryFromJson(String str) =>
    Repository.fromJson(json.decode(str));

String repositoryToJson(Repository data) => json.encode(data.toJson());

class Repository {
  int totalCount;
  bool incompleteResults;
  List<Item> items;

  Repository({
    required this.totalCount,
    required this.incompleteResults,
    required this.items,
  });

  factory Repository.fromJson(Map<String, dynamic> json) => Repository(
        totalCount: json["total_count"] ?? 0,
        incompleteResults: json["incomplete_results"] ?? false,
        items: List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "total_count": totalCount,
        "incomplete_results": incompleteResults,
        "items": List<dynamic>.from(items.map((x) => x.toJson())),
      };
}

class Item {
  int id;
  String nodeId;
  String name;
  String fullName;
  bool private;
  Owner owner;
  String htmlUrl;
  String description;
  String url;
  String forksUrl;
  int size;
  int stargazersCount;
  int watchersCount;
  int forksCount;
  String language;
  String createdAt;
  String updatedAt;
  String pushedAt;
  String cloneUrl;

  Item(
      {required this.id,
      required this.nodeId,
      required this.name,
      required this.fullName,
      required this.private,
      required this.owner,
      required this.htmlUrl,
      required this.description,
      required this.url,
      required this.forksUrl,
      required this.size,
      required this.stargazersCount,
      required this.watchersCount,
      required this.forksCount,
      required this.language,
      required this.createdAt,
      required this.updatedAt,
      required this.pushedAt,
      required this.cloneUrl});

  factory Item.fromJson(Map<String, dynamic> json) => Item(
      id: json["id"] ?? "",
      nodeId: json["node_id"] ?? "",
      name: json["name"] ?? "",
      fullName: json["full_name"] ?? "",
      private: json["private"] ?? "",
      owner: Owner.fromJson(json["owner"]),
      htmlUrl: json["html_url"] ?? "",
      description: json["description"] ?? "",
      url: json["url"] ?? "",
      forksUrl: json["forks_url"] ?? "",
      size: json["size"] ?? "",
      stargazersCount: json["stargazers_count"] ?? "",
      watchersCount: json["watchers_count"] ?? "",
      forksCount: json["forks_count"] ?? "",
      language: json["language"] ?? "",
      createdAt: json["created_at"] ?? "",
      updatedAt: json["updated_at"] ?? "",
      pushedAt: json["pushed_at"] ?? "",
      cloneUrl: json["clone_url"] ?? "");

  Map<String, dynamic> toJson() => {
        "id": id,
        "node_id": nodeId,
        "name": name,
        "full_name": fullName,
        "private": private,
        "owner": owner.toJson(),
        "html_url": htmlUrl,
        "description": description,
        "url": url,
        "forks_url": forksUrl,
        "size": size,
        "stargazers_count": stargazersCount,
        "watchers_count": watchersCount,
        "forks_count": forksCount,
        "language": language,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "pushed_at": pushedAt,
        "clone_url": cloneUrl
      };
}

class Owner {
  String login;
  int id;
  String nodeId;
  String avatarUrl;
  String gravatarId;
  String url;

  Owner({
    required this.login,
    required this.id,
    required this.nodeId,
    required this.avatarUrl,
    required this.gravatarId,
    required this.url,
  });

  factory Owner.fromJson(Map<String, dynamic> json) => Owner(
        login: json["login"] ?? "",
        id: json["id"] ?? "",
        nodeId: json["node_id"] ?? "",
        avatarUrl: json["avatar_url"] ?? "",
        gravatarId: json["gravatar_id"] ?? "",
        url: json["url"] ?? "",
      );

  Map<String, dynamic> toJson() => {
        "login": login,
        "id": id,
        "node_id": nodeId,
        "avatar_url": avatarUrl,
        "gravatar_id": gravatarId,
        "url": url,
      };
}
