import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_repository/model/repository.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('Fetch Repo data', () async {
    try {
      final Dio _dio = Dio();
      final response = await _dio
          .get('https://api.github.com/search/repositories', queryParameters: {
        'q': 'flutter',
        'page': 1,
        'per_page': 10,
        'sort': 'sort',
        'order': 'asc'
      });

      debugPrint("RES: ${response.data}");

      expect(response.statusCode, 200);

      return Repository.fromJson(response.data);
    } on DioError catch (e) {
      if (e.type == DioErrorType.other && e.error is SocketException) {
        throw Exception("Failed to fetch repo: No internet connection");
      } else {
        throw Exception("Failed to fetch repo: ${e.message}");
      }
    } catch (e) {
      throw Exception("Failed to fetch repo: ${e.toString()}");
    }
  });
}
